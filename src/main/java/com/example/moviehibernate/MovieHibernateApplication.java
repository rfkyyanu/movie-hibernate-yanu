package com.example.moviehibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieHibernateApplication.class, args);
	}

}
